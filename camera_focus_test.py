import numpy as np
import cv2
import threading
import time


class Camera:
    def __init__(self, camera_number=0):
        self.kill = False
        self.lastFrame = None
        self.captureDevice = cv2.VideoCapture(camera_number)
        # All done, release device
        self.captureDevice.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        # 2048x1536  ve altı hızlı focus, 2592x1944 ve 3264x2448 orta hızlı focus, 4192x3104   çok yavaş
        self.captureDevice.set(cv2.CAP_PROP_FRAME_WIDTH, 4192)
        self.captureDevice.set(cv2.CAP_PROP_FRAME_HEIGHT, 3104)
        # https://store.spinelelectronics.com/USB_2.0_Camera/UC130MPA_AF
        self.cameraLoop = threading.Thread(target=self.camera_loop)
        self.cameraLoop.daemon = True
        self.cameraLoop.start()

    def camera_loop(self):
        while not self.kill:
            ret, frame = self.captureDevice.read()
            # print(ret, time.time(), frame.shape)
            if ret:
                # if self.lastFrame is not None:
                #     diff = np.absolute(frame - self.lastFrame)
                #     print(np.mean(diff))
                self.lastFrame = frame.copy()
                laplacian_var = cv2.Laplacian(self.lastFrame, cv2.CV_64F).var()
                print(laplacian_var)
                wh = frame.shape
                w = wh[1]
                h = wh[0]
                r = w/h
                h = 720
                w = int(r*h)

                cv2.imshow("capture", cv2.resize(self.lastFrame, (w, h)))
                cv2.waitKey(10)

            time.sleep(0.010)


cam = Camera(camera_number=1)

while True:
    time.sleep(1)