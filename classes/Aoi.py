import numpy as np
import cv2
import threading
import time
from classes.ConfigParser import ConfigParser
from classes.PrinterController import PrinterController
from classes.Camera import Camera
import os
import base64


class Aoi:
    def __init__(self):
        print("aoi inited")
        self.isReady = False
        config_file_path = ""
        for x in os.path.abspath(__file__).split("\\")[:-1]:
            config_file_path += x + "/"

        config_file_path += "config"

        self.config = ConfigParser(config_file_path)
        scale_x = self.config.get_scale_x()
        scale_y = self.config.get_scale_y()
        scale_z = self.config.get_scale_z()
        pid = self.config.get_pid()
        vid = self.config.get_vid()
        baudrate = self.config.get_baudrate()
        camera_number = self.config.get_camera_number()
        i2c_slave_address = self.config.get_i2c_slave_address()
        self.cameraStep = self.config.get_camera_step()
        self.cameraXMax = self.config.get_camera_x_max()
        self.cameraXMin = self.config.get_camera_x_min()
        self.cameraYMax = self.config.get_camera_y_max()
        self.cameraYMin = self.config.get_camera_y_min()
        self.cameraZMax = self.config.get_camera_z_max()
        self.cameraZMin = self.config.get_camera_z_min()
        self.preview = self.config.is_preview_enabled()
        self.focusOffset = self.config.focus_offset()
        self.focusStep = self.config.focus_step()

        self.camera = Camera(camera_number=camera_number, preview=self.preview, focus_offset=self.focusOffset, focus_step=self.focusStep)
        self.printer = PrinterController(scale_x=scale_x, scale_y=scale_y, scale_z=scale_z,
                                         pid=pid, vid=vid, baud_rate=baudrate, i2c_slave_address=i2c_slave_address)
        self.isReady = True

    def return_home(self):
        self.camera.set_focus(0)
        self.printer.set_up()

    @staticmethod
    def is_the_same_location(p0, p1):
        ret = True
        for n in range(3):
            ret = ret and (int(p0[n]) == int(p1[n]))
        # print(ret)
        return ret

    def go_to_coordinate(self, x, y, z, wait=False, timeout=10, focus_time=1, select_best=False):
        if self.isReady:
            self.isReady = False
            x = min(max(x, self.cameraXMin), self.cameraXMax)
            y = min(max(y, self.cameraYMin), self.cameraYMax)
            z = min(max(z, self.cameraZMin), self.cameraZMax)
            self.camera.set_focus(z)
            self.printer.go_to(x, y, z, f=2000)

            if wait:
                t0 = time.time()
                while not self.is_the_same_location([x, y, z], self.printer.get_last_known_position()):
                    time.sleep(0.1)
                    if time.time() - t0 > timeout:
                        self.isReady = True
                        return {'message': 'timeout'}

                time.sleep(focus_time)
                # self.camera.save_last_sample("X{:.6f}Y{:.6f}Z{:.6f}".format(x, y, z))
                self.isReady = True
                return {'message': 'done',
                        'frame': self.camera.get_last_frame(),
                        'coordinate': [x, y, z]}
            else:
                self.isReady = True
                return {'message': 'in_progress'}

        else:
            return {'message': 'busy'}

    def set_led(self, index, r, g, b, a):
        r = min(max(r, 0), 255)
        g = min(max(g, 0), 255)
        b = min(max(b, 0), 255)
        a = min(max(a, 0), 100)
        index = min(max(index, 1), 5)

        r = int(r * a / 100)
        g = int(g * a / 100)
        b = int(b * a / 100)

        self.printer.set_led(index, r, g, b, a)
        return {'message': 'done'}

    def get_current_frame(self):
        return {'message': 'done',
                'frame': self.camera.get_last_frame(),
                'coordinate': self.printer.get_last_known_position()}

    def get_current_position(self):
        return {'message': 'done',
                'frame': "none",
                'coordinate': self.printer.get_last_known_position()}