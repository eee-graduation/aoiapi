class ConfigParser:
    def __init__(self, config_file_path):
        self.config = {}
        config_file = open(config_file_path, "r")

        for line in config_file.readlines():
            if line.find("#") > -1:
                line1 = line.split("#")[-1]
            else:
                line1 = line

            line1 = line1.replace(" ", "").replace("\n", "").replace("\r", "")
            if len(line1) > 0 and line1.find("=") > -1:
                key_value = line1.split("=")
                self.config[key_value[0]] = key_value[1]
        config_file.close()

    def get_scale_x(self):
        default = 1
        if "scale_x" in self.config.keys():
            try:
                scale_x = float(self.config["scale_x"])
            except:
                scale_x = default
        else:
            scale_x = default

        return scale_x

    def get_scale_y(self):
        default = 1
        if "scale_y" in self.config.keys():
            try:
                scale_y = float(self.config["scale_y"])
            except:
                scale_y = default
        else:
            scale_y = default

        return scale_y

    def get_scale_z(self):
        default = 1
        if "scale_z" in self.config.keys():
            try:
                scale_z = float(self.config["scale_z"])
            except:
                scale_z = default
        else:
            scale_z = default

        return scale_z

    def get_pid(self):
        default = 9025
        if "pid" in self.config.keys():
            try:
                pid = int(self.config["pid"])
            except:
                pid = default
        else:
            pid = default

        return pid

    def get_vid(self):
        default = 66
        if "vid" in self.config.keys():
            try:
                vid = int(self.config["vid"])
            except:
                vid = default
        else:
            vid = default

        return vid

    def get_baudrate(self):
        default = 115200
        if "baudrate" in self.config.keys():
            try:
                baudrate = int(self.config["baudrate"])
            except:
                baudrate = default
        else:
            baudrate = default

        return baudrate

    def get_camera_number(self):
        default = 0
        if "camera_number" in self.config.keys():
            try:
                camera_number = int(self.config["camera_number"])
            except:
                camera_number = default
        else:
            camera_number = default

        return camera_number

    def get_camera_step(self):
        default = 0
        if "camera_step" in self.config.keys():
            try:
                camera_step = int(self.config["camera_step"])
            except:
                camera_step = default
        else:
            camera_step = default

        return camera_step

    def get_camera_x_min(self):
        default = 0
        if "camera_x_min" in self.config.keys():
            try:
                camera_x_min = int(self.config["camera_x_min"])
            except:
                camera_x_min = default
        else:
            camera_x_min = default

        return camera_x_min

    def get_camera_x_max(self):
        default = 0
        if "camera_x_max" in self.config.keys():
            try:
                camera_x_max = int(self.config["camera_x_max"])
            except:
                camera_x_max = default
        else:
            camera_x_max = default

        return camera_x_max

    def get_camera_y_min(self):
        default = 0
        if "camera_y_min" in self.config.keys():
            try:
                camera_y_min = int(self.config["camera_y_min"])
            except:
                camera_y_min = default
        else:
            camera_y_min = default

        return camera_y_min

    def get_camera_y_max(self):
        default = 0
        if "camera_y_max" in self.config.keys():
            try:
                camera_y_max = int(self.config["camera_y_max"])
            except:
                camera_y_max = default
        else:
            camera_y_max = default

        return camera_y_max

    def get_camera_z_min(self):
        default = 0
        if "camera_z_min" in self.config.keys():
            try:
                camera_z_min = int(self.config["camera_z_min"])
            except:
                camera_z_min = default
        else:
            camera_z_min = default

        return camera_z_min

    def get_camera_z_max(self):
        default = 0
        if "camera_z_max" in self.config.keys():
            try:
                camera_z_max = int(self.config["camera_z_max"])
            except:
                camera_z_max = default
        else:
            camera_z_max = default

        return camera_z_max

    def get_i2c_slave_address(self):
        default = 99
        if "i2c_slave_address" in self.config.keys():
            try:
                i2c_slave_address = int(self.config["i2c_slave_address"])
            except:
                i2c_slave_address = default
        else:
            i2c_slave_address = default

        return i2c_slave_address

    def is_preview_enabled(self):
        default = False
        if "preview" in self.config.keys():
            try:
                preview = int(self.config["preview"]) == 1
            except:
                preview = default
        else:
            preview = default

        return preview

    def focus_offset(self):
        default = 450
        if "focus_offset" in self.config.keys():
            try:
                focus_offset = int(self.config["focus_offset"])
            except:
                focus_offset = default
        else:
            focus_offset = default

        return focus_offset

    def focus_step(self):
        default = 50
        if "focus_step" in self.config.keys():
            try:
                focus_step = int(self.config["focus_step"])
            except:
                focus_step = default
        else:
            focus_step = default

        return focus_step
