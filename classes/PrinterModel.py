import serial
import serial.tools.list_ports
import threading
import time


class PrinterModel:
    def __init__(self, scale_x=1, scale_y=1, scale_z=1, pid=9025, vid=66, baud_rate=115200):
        self._demand_position_count = 0
        self._demandList = []
        self.pid = pid
        self.vid = vid
        self.baudRate = baud_rate
        device = self.get_tty_dev()
        assert device is not None
        self.device = device
        # print("self.serial = serial.Serial(self.device, self.baudRate, timeout=1)")
        self.serial = serial.Serial(self.device, self.baudRate, timeout=1)
        self.readThread = threading.Thread(target=self.serial_loop)
        self.readThread.daemon = True
        self.readThread.start()
        self.lastKnownLocation = [0, 0, 0]
        time.sleep(10)
        self._isAvailable = True

    def demand(self, code):
        self._demandList.append(code)

    def clear_demands(self):
        self._demandList.clear()

    def get_demand_length(self):
        return len(self._demandList)

    def serial_loop(self):
        while True:
            self._demand_position_count += 1
            self._isAvailable = True
            time.sleep(0.1)
            if self.serial.in_waiting > 0:
                message = self.serial.read(self.serial.in_waiting).decode("utf-8", "ignore")
                print(message)
                self.parse_position(message)
                if message.find("Busy") > -1:
                    time.sleep(1)
                    self._isAvailable = False

            if self._isAvailable and self._demand_position_count > 10:
                self._demand_position_count = 0
                self._demand_position()

            if self._isAvailable and len(self._demandList) > 0:
                code = self._demandList.pop(0)
                for line in code.split("\r\n"):
                    print(line + "\r\n")
                    time.sleep(0.1)
                    self._send(line + "\r\n")

    def parse_position(self, message):
        x, y, z = None, None, None
        for line in message.split("\r\n"):
            if line.find("Count") > -1:
                line2 = line.split("Count")[0]
                if len(line2)>0 and line2[0] == "X":
                    x = float(line2.split("Y:")[0].replace("X:", ""))
                    y = float(line2.split("Z:")[0].split("Y:")[1].replace("Y:", ""))
                    z = float(line2.split("Z:")[1].split("E:")[0].replace("Z:", ""))

        if x is not None:
            self._demand_position_count = 0
            self.lastKnownLocation = [x, y, z]

    def _demand_position(self):
        code = self._get_position_demand_str()
        self._send(code)

    @staticmethod
    def _get_position_demand_str():
        return "M114\r\n"

    def _send(self, message):
        try:
            if type(message) == list:
                message = serial.to_bytes(message)
            elif type(message) == str:
                message = message.encode()
            else:
                self.serial.write(message)
            for line in message.split(b"\r\n"):
                self.serial.write(line + b'\r\n')
                time.sleep(0.010)
        except:
            print("serial write error !!!")


    @staticmethod
    def get_tty_dev():
        a_list = list(serial.tools.list_ports.comports())
        print(a_list)
        for tty in a_list:
            if tty.pid == 66 and tty.vid == 9025:
                print(tty.device)
                return tty.device
        return "COM3"
