import numpy as np
import cv2
import threading
import time
import base64


class Camera:
    def __init__(self, camera_number=0, preview=False,  focus_offset=450, focus_step=50):
        self.kill = False
        self.lastFrame = None
        self.lastGrayFrame = None
        self.preview = preview
        self.focusOffset = focus_offset
        self.focusStep = focus_step
        self.count = 0
        self.captureDevice = cv2.VideoCapture(camera_number)
        self.captureDevice.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
        # 2048x1536  ve altı hızlı focus, 2592x1944 ve 3264x2448 orta hızlı focus, 4192x3104   çok yavaş
        self.captureDevice.set(cv2.CAP_PROP_FRAME_WIDTH, 4192)
        self.captureDevice.set(cv2.CAP_PROP_FRAME_HEIGHT, 3104)
        self.captureDevice.set(cv2.CAP_PROP_AUTOFOCUS, 0)
        self.captureDevice.set(cv2.CAP_PROP_FOCUS, self.focusOffset) # 400-500@ z=0, 500-550*@z=2,
        # print("--------------------->", self.captureDevice.get(cv2.CAP_PROP_FOCUS)) # 0.0
        # print("--------------------->", self.captureDevice.get(cv2.CAP_PROP_AUTOFOCUS)) # 0.0

        # https://store.spinelelectronics.com/USB_2.0_Camera/UC130MPA_AF
        self.cameraLoop = threading.Thread(target=self.camera_loop)
        self.cameraLoop.daemon = True
        self.cameraLoop.start()

    def set_focus(self, z_value_as_mm):
        print(self.focusOffset + int(self.focusStep/10*z_value_as_mm),"------------")
        self.captureDevice.set(cv2.CAP_PROP_FOCUS, self.focusOffset + int(self.focusStep/10*z_value_as_mm))

    def camera_loop(self):
        while not self.kill:
            ret, frame = self.captureDevice.read()
            # print(ret, time.time(), frame.shape)
            if ret:
                # if self.lastFrame is not None:
                #     difference = abs(self.lastFrame - frame)
                #     print(np.mean(difference))
                self.lastFrame = frame.copy()
                wh = frame.shape
                w = wh[1]
                h = wh[0]
                r = w/h
                h = 720
                w = int(r*h)
                # self.captureDevice.set(cv2.CAP_PROP_FOCUS, self.count)
                # print("--------------------->", self.captureDevice.get(cv2.CAP_PROP_FOCUS))  # 0.0
                # self.count += 10


                # print(laplacian_var)
                # kernel = np.ones((5, 5), np.float32) / 25
                # gray = cv2.cvtColor(self.lastFrame, cv2.COLOR_RGB2GRAY)
                # laplacian_var = cv2.Laplacian(gray, cv2.CV_64F).var()

                # black_value = np.mean(gray)
                # gray = cv2.calcHist([gray], [0], None, [256], [0, 256])
                # gray = cv2.filter2D(gray, -1, kernel)

                # if self.lastGrayFrame is not None:
                #     # difference = np.mean(abs(gray - self.lastGrayFrame))
                #     difference = cv2.matchTemplate(self.lastGrayFrame, gray, cv2.TM_CCOEFF_NORMED)[0][0]
                #     img_hist_diff = cv2.compareHist(self.lastGrayFrame, gray, cv2.HISTCMP_BHATTACHARYYA)
                #     print("{:.5f}, {:.5f}, {:.5f}, {:.5f}".format(laplacian_var, black_value, difference, img_hist_diff))
                #     if black_value > 60 and difference > 0.98 and img_hist_diff < 0.15:
                #         self.count += 1
                #         if self.count > 5:
                #             print("focused !!!")
                #     else:
                #         self.count = 0

                # self.lastGrayFrame = gray

                if self.preview:
                    cv2.imshow("capture", cv2.resize(self.lastFrame, (w, h)))
                    cv2.waitKey(1)

            time.sleep(0.010)

        self.captureDevice.release()
        cv2.destroyAllWindows()

    def get_last_frame(self, x=0, y=0, z=0):
        if self.lastFrame is None:
            return "none"
        name = "X{:.6f}Y{:.6f}Z{:.6f}".format(x, y, z)
        destination = "storage/" + name + ".png"
        cv2.imwrite(destination, self.lastFrame)
        # _, png_frame = cv2.imencode('.png', self.lastFrame)
        # return base64.b64encode(png_frame).decode()
        return destination

    def save_last_sample(self, name):
        if self.lastFrame is None:
            return None
        cv2.imwrite("storage/" + name + ".png", self.lastFrame)
        return 1


# c = Camera(camera_number=1, preview=True)
#
# while True:
#     time.sleep(1)