from classes.PrinterModel import PrinterModel
from classes.Gcode import GCode


class PrinterController:
    def __init__(self, scale_x=1, scale_y=1, scale_z=1, pid=9025, vid=66, baud_rate=115200, i2c_slave_address=99):
        self.i2cSlaveAddress = i2c_slave_address
        self.model = PrinterModel(scale_x=scale_x, scale_y=scale_y, scale_z=scale_z,
                                  pid=pid, vid=vid, baud_rate=baud_rate)
        self.gCode = GCode()
        self.set_up()

    def set_up(self):
        code = self.gCode.set_space_str()
        code += self.gCode.return_home_str()
        self.model.demand(code)

    def go_to(self, x, y, z, f=None):
        code = self.gCode.set_position_str(x, y, z, f=f)
        self.model.demand(code)

    def get_last_known_position(self):
        return self.model.lastKnownLocation

    def set_led(self, index, r, g, b, a):
        # r = int(r * a / 100)
        # g = int(g * a / 100)
        # b = int(b * a / 100)
        code = self.gCode.i2c_send(self.i2cSlaveAddress, [index, r, g, b])
        print(code)
        self.model.demand(code)

