class GCode:
    def __init__(self):
        # https://marlinfw.org/docs/gcode/G000-G001.html
        pass

    @staticmethod
    def set_space_str():
        # set units to mm
        # set plane to XY
        return "\r\n\r\nG21\r\nG17\r\n"

    @staticmethod
    def set_position_str(x, y, z, f=None):
        if f is None:
            return "G01 X{:.6f} Y{:.6f} Z{:.6f}".format(x, y, z)
        else:
            return "G01 X{:.6f} Y{:.6f} Z{:.6f} F{:.6f}".format(x, y, z, f)

    @staticmethod
    def return_home_str():
        # return "G00 Z0\r\nG00 X0 Y0\r\n"
        return "G28 Z\r\nG28 X Y"

    @staticmethod
    def get_position_str():
        return "M114\r\n"

    @staticmethod
    def i2c_send(address, data):
        code = "M260 R1" + "\r\n"
        code += "M260 A" + str(address) + "\r\n"
        if type(data) == str:
            data = [ord(x) for x in data]

        for x in data:
            code += "M260 B" + str(x) + "\r\n"

        code += "M260 S1 \r\n"
        return code

