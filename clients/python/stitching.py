# https://medium.com/analytics-vidhya/image-stitching-with-opencv-and-python-1ebd9e0a6d78
# https://mono.software/2018/03/14/Image-stitching/
# https://walchko.github.io/blog/computer-vision/Image-Stitching/Image-Stitching.html
import cv2
import numpy as np
import matplotlib.pyplot as plt


cv2.ocl.setUseOpenCL(False)

imgs2 = [cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/1_image_at_ x125y75z0.png'),
         cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/2_image_at_ x125y100z0.png'),
         cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/3_image_at_ x150y125z0.png'),
         cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/4_image_at_ x150y100z0.png'),
         cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/5_image_at_ x175y75z0.png'),
         cv2.imread('C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage/6_image_at_ x175y100z0.png')]

# display the individual images
plt.figure(figsize=(9,6))  # width, height
for i, img in enumerate(imgs2):
    plt.subplot(2,3,i+1)
    plt.imshow(img)
    h,w,_ = img.shape
    plt.title('{}x{}'.format(w,h))
    plt.xticks(())
    plt.yticks(())

stitcher = cv2.createStitcher(True)  # use the GPU
err, result = stitcher.stitch(imgs2)
print(err)
if err:
    print("crap!")

# save a high res copy to look at
# ans = cv2.cvtColor(result, cv2.COLOR_RGB2BGR)
ans = result
cv2.imwrite('office.png', ans)

# display
plt.subplot(2,3,2+1)
plt.imshow(result)
h, w, _ = ans.shape
plt.title('{}x{}'.format(w, h))
# plt.show()