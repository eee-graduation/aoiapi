import requests
import cv2
import numpy
from skimage import io
import time


class Client:
    def __init__(self):
        self._led_indexes = [x for x in range(1, 6)]

    def set_white(self, brightness):
        for idx in self._led_indexes:
            self.set_led(idx, 255, 255, 255, brightness)

    def tun_off_led(self, index):
        return self.set_led(index, 0, 0, 0, 0)

    def reverse_scan(self, x_min, x_max, y_min, y_max, z, step, timeout=100, focus_time=6):
        wait = 1
        x = x_max
        out = []

        while x >= x_min:
            for y in range(y_max, y_min, -step):
                data = self.go_to_coordinate(x, y, z, wait, timeout, focus_time)
                out.append(data)

            x -= step

            if x <= x_max:
                for y in range(y_min, y_max, step):
                    data = self.go_to_coordinate(x, y, z, wait, timeout, focus_time)
                    out.append(data)

            x -= step

        return out

    def scan(self, x_min, x_max, y_min, y_max, z, step, timeout=100, focus_time=6):
        wait = 1
        x = x_min
        out = []

        while x <= x_max:
            for y in range(y_min, y_max, step):
                data = self.go_to_coordinate(x, y, z, wait, timeout, focus_time)
                out.append(data)

            x += step

            if x <= x_max:
                for y in range(y_max, y_min, -step):
                    data = self.go_to_coordinate(x, y, z, wait, timeout, focus_time)
                    out.append(data)

            x += step

        return out

    def go_to_coordinate(self, x, y, z, wait=1, timeout=100, focus_time=6):
        keys = ['x', 'y', 'z', 'wait', 'timeout', 'focus_time']
        values = [x, y, z, wait, timeout, focus_time]
        data = self._request('go_to_coordinate', keys, values)
        return data

    def get_current_frame(self, debug=False):
        data = self._request('get_current_frame', [], [])
        if debug:
            cv2.imshow("frame", data["frame"])
            cv2.waitKey()
        return data

    def get_current_position(self):
        data = self._request('get_current_position', [], [])
        return data

    def set_led(self, index, r, g, b, a):
        keys = ['index', 'r', 'g', 'b', 'a']
        values = [index, r, g, b, a]
        data = self._request("set_led", keys, values)
        return data

    def _request(self, function_name, keys, values, req_timeout=20):
        base_url = 'http://127.0.0.1:5000/'
        url = base_url + function_name + '?'
        N = len(keys)
        if N > 0:
            for n in range(N):
                url += keys[n] + '=' + str(values[n]) + '&'

        res = requests.get(url, verify=False, timeout=req_timeout)
        if res.ok:
            data = res.json()
            print(data)
            if len(data) > 0:
                if "frame" in data and not data["frame"] == "none":
                    frame = io.imread(base_url + data["frame"])
                    data["frame"] = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                return data
        return None

    @staticmethod
    def save_as_image(out, path):
        for n, data in enumerate(out):
            print(data.keys())
            tags = ["x", "y", "z"]
            cords = ""
            for x, tag in zip(data["coordinate"], tags):
                cords += tag + str(x)
            cv2.imwrite(path + "/" + str(n+1) + "_image_at_ " + cords + '.png', data["frame"])


c = Client()
# c.set_led(1, 255, 0, 0, 255)
# c.get_current_frame(debug=True)
# c.go_to_coordinate(150, 100, 0)
c.set_white(80)
time.sleep(6)
c.tun_off_led(5)
time.sleep(2)
c.go_to_coordinate(150, 100, 0)
time.sleep(5)

c.go_to_coordinate(150, 100, 10)
time.sleep(5)

c.go_to_coordinate(150, 100, 20)
time.sleep(5)

c.go_to_coordinate(150, 100, 30)
time.sleep(5)

c.go_to_coordinate(150, 100, 35)

# c.save_as_image(c.scan(125, 175, 75, 125, 0, 25, focus_time=1), path="C:/Users/Lab PC 2/OneDrive\Belgeler/aoi_api_v1/clients/python/storage")
# c.reverse_scan(0, 100, 100, 150, 0, 20, focus_time=1)


