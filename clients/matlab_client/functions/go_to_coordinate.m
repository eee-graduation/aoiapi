function [data] = go_to_coordinate(x, y, z, wait, timeout, focus_time)
% x, y, z : coordinates (mm)
% wait: 0=> do not wait, 1=> wait until camera goes to the coordinates
% timeout: wait timeout (seconds)
% focus_time: focus time (seconds)
% return frame if wait=1 else frame=='none'
keys = {'x', 'y', 'z', 'wait', 'timeout', 'focus_time'};
values = [x, y, z, wait, timeout, focus_time];
data = request('go_to_coordinate', keys, values);
%x=50&y=100&z=0&wait=1&timeout=10&focus_time=1
