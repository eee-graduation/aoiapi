function [data] = get_current_frame()
% return current frame
keys = {};
values = [];
data = request('get_current_frame', keys, values);
end

