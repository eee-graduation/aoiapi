function [data] = get_current_position()
% return current position
keys = {};
values = [];
data = request('get_current_position', keys, values);
end

