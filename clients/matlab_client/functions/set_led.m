function [data] = set_led(index, r, g, b, a)
keys = {'index', 'r', 'g', 'b', 'a'};
values = [index, r, g, b, a];
data = request('set_led', keys, values);
end

