function [data] = request(function_name, keys, values)
options = weboptions('Timeout',20);
base_url = 'http://127.0.0.1:5000/';
url = [base_url function_name '?'];
N = length(keys);
if N>0
    for n=1:N
        url = [url keys{n} '=' num2str(values(n)) '&'];
    end
end
data = webread(url(1:(end-1)), options);
if sum(strcmp(fieldnames(data), 'frame')) == 1 && ~strcmp(data.frame,'none')
    data.frame = imread([base_url data.frame]);
end
end

