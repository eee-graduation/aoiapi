function [out] = reverse_scan(xmin,xmax, ymin, ymax, z, step, timeout, focus_time)
% x, y, z : coordinates (mm)
% step: 0=> scan distance (mm)
% timeout: wait timeout (seconds)
% focus_time: focus time (seconds)
% return frames

wait = 1;
x = xmax;
out = [];
%k=1;
while x >= xmin
    for y=ymax:-step:ymin
        data=go_to_coordinate(x, y, z, wait, timeout, focus_time);
        out = [out data];
        %k = k + 1;
    end
    x = x - step;
    if x >= xmin
        for y=ymin:step:ymax
            data=go_to_coordinate(x, y, z, wait, timeout, focus_time);
            out = [out data];
            %k = k + 1;
        end
        x = x - step;
    end
    
end
end

