clc
clear
close all
addpath("functions")
% scan example
colors = [255, 0, 0];
for n = 1:300
    % set leds
    r = colors(mod(n,3)+1);
    g = colors(mod(n+1,3)+1);
    b = colors(mod(n+2,3)+1);
    a = 100;
    for index=1:5
        set_led(index, r, g, b, a)
        %pause(0.01)
    end
end