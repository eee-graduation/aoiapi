clc
clear
close all
addpath("functions")
% scan example

% set leds
r = 255;
g = 255;
b = 255;
a = 80;

% for index=1:4
%     set_led(index, r, g, b, a)
%     pause(2)
% end

% index=3;
% set_led(index, r, g, b, a1)
% pause(2)
% 
% index=4;
% set_led(index, r, g, b, a)
% pause(2)

% index=5;
% set_led(index, 0, 0, 0, 0)
% pause(2)

% %go to a coordinate 
% x = 230; % (mm)
% y = 20; % (mm)
% z = 0; % (mm)
% wait = 1; 
% timeout = 100; % (seconds)
% focus_time = 6; % (seconds)
% data = go_to_coordinate(x, y, z, wait, timeout, focus_time);
% figure
% imshow(data.frame)
% title(strcat("home ", num2str(data.coordinate'))) 
% drawnow

 % scan
xmin = 110; % (mm)
xmax = 230; % (mm)
ymin = 20; % (mm)
ymax = 130; % (mm)
z = 35; % (mm)
step = 25; % (mm)
timeout = 20; % (seconds)
focus_time = 1; % (seconds)
out = scan(xmin,xmax, ymin, ymax, z, step, timeout, focus_time);
save_image = true;

N = length(out);
for n=1:N
    figure
    imshow(out(n).frame)
    if save_image
        imwrite(out(n).frame,strcat("storage/image at ", num2str(out(n).coordinate'), '.png'))
    end
    title(strcat("image @ ", num2str(out(n).coordinate')))
end
