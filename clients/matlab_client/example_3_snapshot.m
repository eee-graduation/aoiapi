clc
clear
close all
addpath("functions")
% example 3

% set leds
r = 255;
g = 255;
b = 255;
a = 100;
for index=1:4
    set_led(index, r, g, b, a)
    pause(1)
end
set_led(5, r, g, b, 0)
pause(1)

% go to a coordinate 
x = 40; % (mm)
y = 40; % (mm)
z = 20; % (mm)
wait = 1; 
timeout = 20; % (seconds)
focus_time = 3; % (seconds)
data = go_to_coordinate(x, y, z, wait, timeout, focus_time);
figure
imshow(data.frame)
title(strcat("home ", num2str(data.coordinate')))
drawnow