import flask
from flask import request, jsonify, send_from_directory
import cv2
from classes.Aoi import Aoi

app = flask.Flask(__name__, static_url_path='')
app.config["DEBUG"] = False
aoi = Aoi()

with app.app_context():
    arg_error_message = jsonify({'message': "argument error."})


@app.route('/', methods=['GET'])
def home():
    return "<h1>AOI</h1>"


# http://127.0.0.1:5000/go_to_coordinate?x=50&y=100&z=0&wait=1&timeout=10&focus_time=1
@app.route('/go_to_coordinate', methods=['GET'])
def go_to_coordinate():
    try:
        if 'x' in request.args and 'y' in request.args and 'z' in request.args:
            x = int(request.args['x'])
            y = int(request.args['y'])
            z = int(request.args['z'])
        else:
            return arg_error_message

        if 'wait' in request.args:
            wait = int(request.args['wait']) == 1
        else:
            wait = True

        if 'timeout' in request.args:
            timeout = int(request.args['timeout'])
        else:
            timeout = 20

        if 'focus_time' in request.args:
            focus_time = int(request.args['focus_time'])
        else:
            focus_time = 1
    except:
        return arg_error_message

    ret = aoi.go_to_coordinate(x, y, z, wait=wait, timeout=timeout, focus_time=focus_time)
    print(ret)
    return jsonify(ret)


# http://127.0.0.1:5000/set_led?index=1&r=255&g=255&b=255&a=255
@app.route('/set_led', methods=['GET'])
def set_led():
    try:
        if 'index' in request.args and \
                'r' in request.args and \
                'g' in request.args and \
                'b' in request.args and \
                'a' in request.args:
            index = int(request.args['index'])
            r = int(request.args['r'])
            g = int(request.args['g'])
            b = int(request.args['b'])
            a = int(request.args['a'])
        else:
            return arg_error_message
    except:
        return arg_error_message
    ret = aoi.set_led(index, r, g, b, a)
    print(ret)
    return jsonify(ret)


# http://127.0.0.1:5000/get_current_frame
@app.route('/get_current_frame', methods=['GET'])
def get_current_frame():
    ret = aoi.get_current_frame()
    print(ret)
    return jsonify(ret)


# http://127.0.0.1:5000/get_current_position
@app.route('/get_current_position', methods=['GET'])
def get_current_position():
    ret = aoi.get_current_position()
    print(ret)
    return jsonify(ret)


@app.route('/storage/<path:path>')
def send_js(path):
    return send_from_directory('storage', path)


app.run()
